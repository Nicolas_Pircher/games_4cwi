package at.pircher.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectangleActor {
	private double x, y;
	private boolean rectUp = false;
	private boolean rectDown = false;
	private boolean rectLeft = false;
	private boolean rectRight = true;

	public RectangleActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void update(GameContainer gc, int delta) {
		if (this.rectRight == true) {
			x++;
		}
		if (x == 700) {
			this.rectRight = false;
			this.rectDown = true;
		}
		if (this.rectDown == true) {
			y++;
		}
		if (y == 500) {
			this.rectDown = false;
			this.rectLeft = true;
		}
		if (this.rectLeft == true) {
			x--;
		}
		if (x == 100) {
			this.rectLeft = false;
			this.rectUp = true;
		}
		if (this.rectUp == true) {
			y--;
		}
		if (y == 100) {
			this.rectUp = false;
			this.rectRight = true;
		}
	}

	public void render(Graphics graphics) {
		graphics.drawRect((float) this.x, (float) this.y, 50, 50);
	}
}
