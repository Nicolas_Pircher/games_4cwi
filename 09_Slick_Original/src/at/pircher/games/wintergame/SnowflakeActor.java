package at.pircher.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import java.util.Random;

public class SnowflakeActor {
	private double x, y;
	private float size;
	private float speed;

	public SnowflakeActor(String size) {
		super();
		RandomXY();
		if (size.equals("large")) {
			this.size = 30;
			this.speed = (float) 1;
		} else if (size.equals("medium")) {
			this.size = 20;
			this.speed = (float) 0.66;
		} else if (size.equals("small")) {
			this.size = 10;
			this.speed = (float) 0.33;
		}
	}

	private void RandomXY() {
		this.x = RandomNumber(800);
		this.y = RandomNumber(600) * -1;
	}

	private int RandomNumber(int range) {
		Random r = new Random();
		return r.nextInt(range);
	}

	public void update(GameContainer gc, int delta) {
		if (y < 700) {
			this.y = this.y + this.speed;
		} else {
			RandomXY();
		}
	}

	public void render(Graphics graphics) {
		graphics.fillOval((float) this.x, (float) this.y, this.size, this.size);
	}
}
