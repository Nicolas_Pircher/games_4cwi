package at.pircher.games.wintergame;

import org.newdawn.slick.AppGameContainer;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;
import java.util.ArrayList;
import java.util.List;

public class MainGame extends BasicGame {
	/*private CircleActor ca1;
	private OvalActor oa1;
	private RectangleActor ra1; */
	private List<SnowflakeActor>Snowflakes;
	private ShootingStar ss1;

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		/*this.ca1.render(graphics);
		this.oa1.render(graphics);
		this.ra1.render(graphics);*/
		this.ss1.render(graphics);
		for(SnowflakeActor s : this.Snowflakes) {
			s.render(graphics);
		}

	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		/*this.ca1 = new CircleActor(400, 100);
		this.oa1 = new OvalActor(100, 500);
		this.ra1 = new RectangleActor(100, 100);*/
		this.ss1 = new ShootingStar(100, 100);
		this.Snowflakes=new ArrayList<>();
		for(int i = 0; i < 20; i++) {
			this.Snowflakes.add(new SnowflakeActor("large"));
			this.Snowflakes.add(new SnowflakeActor("medium"));
			this.Snowflakes.add(new SnowflakeActor("small"));
			
		}

	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		/*this.ca1.update(gc, delta);
		this.oa1.update(gc, delta);
		this.ra1.update(gc, delta);*/
		this.ss1.update(gc, delta);
		for(SnowflakeActor s : this.Snowflakes) {
			s.update(gc, delta);
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
