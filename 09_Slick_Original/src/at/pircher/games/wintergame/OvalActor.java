package at.pircher.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor {
	private double x,y;
	private boolean ovalRight = true;
	private boolean ovalLeft = false;
	
	
	
	public OvalActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	public void update(GameContainer gc, int delta) {
		if(this.ovalRight == true) {
			x++;
		}if (x == 750) {
			this.ovalRight=false;
			this.ovalLeft=true;
		}if(this.ovalLeft == true) {
			x--;
		}if(x==50) {
			this.ovalLeft=false;
			this.ovalRight=true;
		}
	}
	public void render(Graphics graphics) {
		graphics.drawOval((float)this.x, (float)this.y, 40, 20);
	}
}
